# dotfiles

## Installation

```
$ mkdir -p $HOME/work/src/gitlab.com/longkey1
$ git clone git@gitlab.com:longkey1/dotfiles.git $HOME/work/src/gitlab.com/longkey1/dotfiles
$ pushd $HOME/work/src/gitlab.com/longkey1/dotfiles && make build && make install && popd
```
